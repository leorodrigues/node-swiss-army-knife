const { createContainer, Lifetime } = require('awilix');

const container = createContainer();

container.loadModules(
    [ './components/*/src/**/!(index).js', './config/**/!(index)*.js' ],
    {
        formatName: 'camelCase',
        resolverOptions: {
            lifetime: Lifetime.SINGLETON
        }
    });

const subscribeToEvents = container.resolve('subscribeToEvents');

const server = container.resolve('server');

subscribeToEvents();

server.start(process.env, process.argv).catch(e =>
    process.stdout.write(`${e.message}\n`));

const quit = () => process.exit(0);

const printError = e => process.stdout.write(`${e.message}\n`);

process.on('SIGINT', () => server.stop().catch(printError).finally(quit));