
const path = require('path');

const BASE_URN = 'urn:leonardo.org:swiss-army-knife';

const topics = {
    startUpCompleteUrn:`${BASE_URN}:boot-sequence:events#startup-complete`,
    rebootScheduledUrn:`${BASE_URN}:boot-sequence:events#reboot-scheduled`,
    shutdownScheduledUrn:`${BASE_URN}:boot-sequence:events#shutdown-scheduled`,

    hotspotEnabledUrn:`${BASE_URN}:hotspot:events#enabled`,

    networkAcquiredUrn:`${BASE_URN}:network:events#acquired`,
    networkAcquisitionFailedUrn:`${BASE_URN}:network:events#acquisition-failed`,

    wifiProfileLoadedUrn: `${BASE_URN}:profile:events#wifi-loaded`,
    hotspotProfileLoadedUrn: `${BASE_URN}:profile:events#hotspot-loaded`,
    profileSwitchedUrn: `${BASE_URN}:profile:events#switched`,

    wifiEnabledUrn: `${BASE_URN}:wifi:events#enabled`
};

const DB_PATH = process.env.USERPROFILE
    || process.env.USER_HOME
    || process.env.HOME
    || './';

const logger = {
    level: process.env.NODE_ENV === 'production' ? 'warn' : 'debug'
};

const database = {
    path: path.join(DB_PATH, '.data'),
    name: 'san-db'
};

const httpServer = {
    port: parseInt(process.env.PORT || 8080)
};

module.exports = function config() {
    return { topics, logger, database, httpServer };
};