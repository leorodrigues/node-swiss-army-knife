function makeMocks(jest) {

    const stubs = {
        makeWifi: jest.fn(),
        enable: jest.fn(),
    };

    const wifiFactory = {
        makeWifi: stubs.makeWifi
    };

    const wifi = {
        enable: stubs.enable
    };

    return { wifiFactory, wifi, stubs };
}

module.exports = { makeMocks };