const { EnableWifi } = require('../../../src/application');
const { makeMocks } = require('./EnableWifi.mockery');

const { wifiFactory, wifi, stubs } = makeMocks(jest);

const enable = new EnableWifi({ wifiFactory });

describe('components/wifi/application/EnableWifi', () => {
    describe('#invoke()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(async () => {
            stubs.makeWifi.mockReturnValue(wifi);
            await enable.invoke();
        });

        it('Should instantiate a wifi', async () =>
            expect(wifiFactory.makeWifi).toHaveBeenCalledWith());

        it('Should enable the instantiated wifi', async () =>
            expect(wifi.enable).toHaveBeenCalledWith());
    });
});