const { Wifi } = require('../../../src/domain');
const { makeMocks } = require('./Wifi.mockery');

const { transmitter, wifiEvents } = makeMocks(jest);

const wifi = new Wifi(transmitter, wifiEvents);

describe('components/wifi/domain/Wifi', () => {
    describe('#enable()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => wifi.enable());

        it('Should start the receiver', async () =>
            expect(transmitter.start).toHaveBeenCalledWith());

        it('Should emit the "wifiEnabled" event', async () =>
            expect(wifiEvents.emitWifiEnabled)
                .toHaveBeenCalledWith());
    });
});