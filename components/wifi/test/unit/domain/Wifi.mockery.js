function makeMocks(jest) {

    const stubs = {
        start: jest.fn(),
        emitWifiEnabled: jest.fn(),
    };

    const transmitter = {
        start: stubs.start
    };

    const wifiEvents = {
        emitWifiEnabled: stubs.emitWifiEnabled
    };

    return { transmitter, wifiEvents, stubs };
}

module.exports = { makeMocks };