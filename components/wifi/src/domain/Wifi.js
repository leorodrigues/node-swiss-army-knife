module.exports = class Wifi {
    constructor(transmitter, wifiEvents) {
        this.transmitter = transmitter;
        this.wifiEvents = wifiEvents;
    }

    async enable() {
        await this.transmitter.start();
        return this.wifiEvents.emitWifiEnabled();
    }
};