const Wifi = require('./Wifi');

/* istanbul ignore next */
module.exports = class DefaultWifiFactory {
    constructor({ transmitter, wifiEvents }) {
        this.transmitter = transmitter;
        this.wifiEvents = wifiEvents;
    }

    makeWifi() {
        return new Wifi(this.transmitter, this.wifiEvents);
    }
};