
module.exports = class EnableWifi {
    constructor({ wifiFactory }) {
        this.wifiFactory = wifiFactory;
    }

    async invoke() {
        const wifi = this.wifiFactory.makeWifi();
        return wifi.enable();
    }
};