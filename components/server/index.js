module.exports = {
    ...require('./src/interface'),
    ...require('./src/system')
};