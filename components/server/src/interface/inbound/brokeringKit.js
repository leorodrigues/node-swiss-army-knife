
const { BrokeringKit } = require('@leorodrigues/mu-broker');
const { Logger } = require('../../system');

module.exports = function brokeringKit({ config }) {
    const logger = new Logger('event-broker', config.logger.level);
    return new BrokeringKit(logger);
};