
module.exports = function eventTopics({ brokeringKit, config }) {
    const {
        startUpCompleteUrn,
        rebootScheduledUrn,
        shutdownScheduledUrn,
        hotspotEnabledUrn,
        networkAcquiredUrn,
        networkAcquisitionFailedUrn,
        wifiProfileLoadedUrn,
        hotspotProfileLoadedUrn,
        profileSwitchedUrn,
        wifiEnabledUrn
    } = config.topics;

    return {
        startUpComplete: brokeringKit.newTopic(startUpCompleteUrn),
        rebootScheduled: brokeringKit.newTopic(rebootScheduledUrn),
        shutdownScheduled: brokeringKit.newTopic(shutdownScheduledUrn),
        hotspotEnabled: brokeringKit.newTopic(hotspotEnabledUrn),
        networkAcquired: brokeringKit.newTopic(networkAcquiredUrn),
        wifiProfileLoaded: brokeringKit.newTopic(wifiProfileLoadedUrn),
        hotspotProfileLoaded: brokeringKit.newTopic(hotspotProfileLoadedUrn),
        profileSwitched: brokeringKit.newTopic(profileSwitchedUrn),
        wifiEnabled: brokeringKit.newTopic(wifiEnabledUrn),
        networkAcquisitionFailed:
            brokeringKit.newTopic(networkAcquisitionFailedUrn),
    };
};