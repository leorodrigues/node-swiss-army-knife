const express = require('express');
const Controller = require('./Controller');
const { Logger } = require('../../system');

module.exports = class ConnectionController extends Controller {
    constructor({
        purgeConnections,
        createConnection,
        deleteConnection,
        updateConnection,
        getConnections,
        config
    }) {
        super(new Logger('connection-controller', config.logger.level));
        this.purgeConnections = purgeConnections;
        this.createConnection = createConnection;
        this.updateConnection = updateConnection;
        this.deleteConnection = deleteConnection;
        this.getConnections = getConnections;
    }

    makeRouter() {
        const router = express.Router();
        router.get('/connection', this.handleGetConnections());
        router.post('/connection', this.handleCreateConnection());
        router.put('/connection/:id', this.handleUpdateConnection());
        router.delete('/connection/:id', this.handleDeleteConnection());
        router.delete('/connection', this.handleDeleteConnections());
        router.use(this.handleHotFound());
        router.use(this.handleErrors());
        return router;
    }

    handleGetConnections() {
        const command = this.getConnections;
        return async (request, response, next) => {
            try {
                const result = await command.invoke();
                response.json(result);
                response.end();
            } catch(error) {
                next(error);
            }
        };
    }

    handleCreateConnection() {
        const command = this.createConnection;
        return async (request, response, next) => {
            try {
                const { ssid, password, hidden } = request.body;
                const result = await command.invoke(ssid, password, hidden);
                response.json(result);
                response.end();
            } catch(error) {
                next(error);
            }
        };
    }

    handleUpdateConnection() {
        const command = this.updateConnection;
        return async (request, response, next) => {
            try {
                const id = request.params.id;
                const { ssid, password, hidden } = request.body;
                const result = await command.invoke(id, ssid, password, hidden);
                response.json(result);
                response.end();
            } catch(error) {
                next(error);
            }
        };
    }

    handleDeleteConnection() {
        const command = this.deleteConnection;
        return async (request, response, next) => {
            try {
                await command.invoke(request.params.id);
                response.status(204);
                response.end();
            } catch(error) {
                next(error);
            }
        };
    }

    handleDeleteConnections() {
        const command = this.purgeConnections;
        return async (request, response, next) => {
            try {
                await command.invoke();
                response.status(204);
                response.end();
            } catch(error) {
                next(error);
            }
        };
    }
};