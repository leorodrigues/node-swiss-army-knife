module.exports = class Controller {
    constructor(logger) {
        this.logger = logger;
    }

    handleHotFound() {
        return async (request, response) => {
            response.status(404);
            response.json({ message: 'not found' });
            this.logger.debug({
                tags: [ 'not-found' ],
                map: { url: request.url }
            });
        };
    }

    handleErrors() {
        return async (error, request, response, next) => {
            response.status(500);
            response.json({ message: error.message });
            this.logger.error({
                tags: error.tags,
                map: { message: error.message }
            });
        };
    }
};