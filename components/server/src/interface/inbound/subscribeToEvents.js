
function subscribe(broker, topic, command, transform = e => e) {
    broker.subscribe(topic, {
        handle: (evt, ack) =>
            command.invoke(transform(evt)).finally(() => ack.confirm())
    });
}

module.exports = function subscribeToEvents({
    eventBroker: broker,
    eventTopics: topics,
    loadCurrentProfile,
    acquireNetwork,
    enableHotspot,
    fireReboot,
    enableWifi
}) {
    return () => {
        subscribe(broker, topics.startUpComplete, loadCurrentProfile);
        subscribe(broker, topics.wifiProfileLoaded, enableWifi);
        subscribe(broker, topics.hotspotProfileLoaded, enableHotspot);
        subscribe(broker, topics.wifiEnabled, acquireNetwork);
        subscribe(broker, topics.profileSwitched, fireReboot);
    };
};