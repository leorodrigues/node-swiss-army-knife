
const { BrokeringKit } = require('@leorodrigues/mu-broker');
const { Logger } = require('../../system');

class EventBrokerSpy {
    constructor(delegate, logger) {
        this.delegate = delegate;
        this.logger = logger;
    }

    async subscribe(topic, handler) {
        this.logger.debug(EventBrokerSpy.logMessage(topic, 'subscribe'));
        return this.delegate.subscribe(topic, handler);
    }

    async publish(topic, event) {
        this.logger.debug(EventBrokerSpy.logMessage(topic, 'publish'));
        return this.delegate.publish(topic, event);
    }

    static logMessage(topic, ...tags) {
        return { tags, map: { topic: topic.urn.toURNString() } };
    }
}

module.exports = function eventBroker({ config }) {
    const logger = new Logger('event-broker', config.logger.level);
    const brokeringKit = new BrokeringKit(logger);
    return new EventBrokerSpy(brokeringKit.newMuBroker(), logger);
};