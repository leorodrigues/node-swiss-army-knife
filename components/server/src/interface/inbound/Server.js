const { Logger } = require('../../system');
const express = require('express');
const bodyParser = require('body-parser');

class Server {
    constructor({
        config,
        fireStartUp,
        fireShutdown,
        connectionController,
        profileController
    }) {
        this.port = config.httpServer.port;
        this.logger = new Logger('server', config.logger.level);
        this.fireStartUp = fireStartUp;
        this.fireShutdown = fireShutdown;
        this.connectionController = connectionController;
        this.profileController = profileController;
    }

    configureControllers(instance) {
        const apiRouter = express.Router();

        apiRouter.use(this.profileController.makeRouter());
        apiRouter.use(this.connectionController.makeRouter());

        instance.use(bodyParser.json());
        instance.use('/swiss-army-knife/api', apiRouter);
    }

    async start() {
        const port = this.port;
        const instance = express();

        this.logger.debug({ tags: [ 'startup', 'comencing' ]});

        this.configureControllers(instance);

        instance.listen(port, async () => {
            this.logger.info({ tags: [ 'listening' ], map: { port } });
            await this.fireStartUp.invoke();
            this.logger.info({ tags: [ 'startup', 'complete' ] });
        });
    }

    async stop() {
        this.logger.info({ tags: [ 'shutdown', 'comencing', 'by-signal' ]});
        await this.fireShutdown.invoke();
        this.logger.info({ tags: [ 'termination', 'complete' ] });
    }
}

module.exports = Server;