const express = require('express');
const Controller = require('./Controller');
const { Logger } = require('../../system');

module.exports = class ProfileController extends Controller {
    constructor({ switchProfile, config }) {
        super(new Logger('profile-controller', config.logger.level));
        this.switchProfile = switchProfile;
    }

    makeRouter() {
        const router = express.Router();
        router.put('/profile/current', this.handlePostProfile());
        return router;
    }

    handlePostProfile() {
        const command = this.switchProfile;
        return async (request, response, next) => {
            try {
                await command.invoke(request.body.name);
                response.status(204);
                response.end();
            } catch(error) {
                next(error);
            }
        };
    }
};