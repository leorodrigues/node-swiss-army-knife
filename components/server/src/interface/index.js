module.exports = {
    ...require('./inbound'),
    ...require('./outbound')
};