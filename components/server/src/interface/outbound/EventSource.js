const moment = require('moment');
const BaseEvent = require('./BaseEvent');

module.exports = class EventSource {
    makeBasicEvent(name) {
        return new BaseEvent(name, moment());
    }
};