const EventSource = require('./EventSource');

module.exports = class BootSequenceEvents extends EventSource {
    constructor({ eventBroker, eventTopics: topics }) {
        super();
        this.eventBroker = eventBroker;
        this.topics = topics;
    }

    async emitStartUpComplete() {
        const event = this.makeBasicEvent('startup-complete');
        this.eventBroker.publish(this.topics.startUpComplete, event);
    }

    async emitRebootScheduled() {
        const event = this.makeBasicEvent('reboot-scheduled');
        this.eventBroker.publish(this.topics.rebootScheduled, event);
    }

    async emitShutdownScheduled() {
        const event = this.makeBasicEvent('shutdown-scheduled');
        this.eventBroker.publish(this.topics.shutdownScheduled, event);
    }
};