module.exports = class BaseEvent {
    constructor(name, timestamp) {
        this.name = name;
        this.timestamp = timestamp;
    }

    makeDeepCopy() {
        return new BaseEvent(this.name, this.timestamp);
    }
};