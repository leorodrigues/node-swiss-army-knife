const EventSource = require('./EventSource');

module.exports = class ProfileEvents extends EventSource {
    constructor({ eventBroker, eventTopics: topics }) {
        super();
        this.eventBroker = eventBroker;
        this.topics = topics;
    }

    async emitProfileSwitched() {
        const event = this.makeBasicEvent('profile-switched');
        this.eventBroker.publish(this.topics.profileSwitched, event);
    }

    async emitWifiProfileLoaded() {
        const event = this.makeBasicEvent('wifi-profile-loaded');
        this.eventBroker.publish(this.topics.wifiProfileLoaded, event);
    }

    async emitHotspotProfileLoaded() {
        const event = this.makeBasicEvent('hotspot-profile-loaded');
        this.eventBroker.publish(this.topics.hotspotProfileLoaded, event);
    }
};