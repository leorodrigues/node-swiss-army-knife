const EventSource = require('./EventSource');

module.exports = class HotspotEvents extends EventSource {
    constructor({ eventBroker, eventTopics: topics }) {
        super();
        this.eventBroker = eventBroker;
        this.topics = topics;
    }

    async emitHotspotEnabled() {
        const event = this.makeBasicEvent('hotspot-enabled');
        this.eventBroker.publish(this.topics.hotspotEnabled, event);
    }
};