const EventSource = require('./EventSource');

module.exports = class WifiEvents extends EventSource {
    constructor({ eventBroker, eventTopics: topics }) {
        super();
        this.eventBroker = eventBroker;
        this.topics = topics;
    }

    async emitWifiEnabled() {
        const event = this.makeBasicEvent('wifi-enabled');
        this.eventBroker.publish(this.topics.wifiEnabled, event);
    }
};