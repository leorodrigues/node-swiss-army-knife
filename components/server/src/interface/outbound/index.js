module.exports = {
    BaseEvent: require('./BaseEvent'),
    WifiEvents: require('./WifiEvents'),
    EventSource: require('./EventSource'),
    HotspotEvents: require('./HotspotEvents'),
    NetworkEvents: require('./NetworkEvents'),
    ProfileEvents: require('./ProfileEvents'),
    BootSequenceEvents: require('./BootSequenceEvents')
};