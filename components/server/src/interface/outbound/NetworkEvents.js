const EventSource = require('./EventSource');

module.exports = class NetworkEvents extends EventSource {
    constructor({ eventBroker, eventTopics: topics }) {
        super();
        this.eventBroker = eventBroker;
        this.topics = topics;
    }

    async emitNetworkAquired() {
        const event = this.makeBasicEvent('network-acquired');
        this.eventBroker.publish(this.topics.networkAcquired, event);
    }

    async emitNetworkAquisitionFailed() {
        const event = this.makeBasicEvent('network-acquisition-failed');
        this.eventBroker.publish(this.topics.networkAcquisitionFailed, event);
    }
};