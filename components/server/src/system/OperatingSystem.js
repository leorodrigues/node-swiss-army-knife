const Logger = require('./Logger');

module.exports = class OperatingSystem {
    constructor() {
        this.logger = new Logger('operating-system');
    }

    async scheduleReboot() {
        this.logger.info('reboot scheduled');
    }

    async scheduleShutdown() {
        this.logger.info('shutdown scheduled');
    }
};