
const uuidByString = require('uuid-by-string');
const path = require('path');
const fs = require('fs');

const USER_PATH = process.env.USERPROFILE
    || process.env.USER_HOME
    || process.env.HOME
    || './';

const COLLECTION_CACHE = { };

function ensureDirectoryExists(path) {
    if (fs.existsSync(path)) return;
    fs.mkdirSync(path, { recursive: true });
}

class DelayedAction {
    constructor() {
        this.timeout = null;
    }

    fire(action) {
        this.timeout = setTimeout(action, 600);
    }

    clearTimeout() {
        clearTimeout(this.timeout);
        return this;
    }
}

class PersistentCollection extends Array {
    constructor(filePath, delayedAction) {
        super();
        this.filePath = filePath;
        this.delayedAction = delayedAction || new DelayedAction();
    }

    push(...elements) {
        super.push(...elements);
        this.delayedSaveSync();
    }

    pop() {
        const element = super.pop();
        this.delayedSaveSync();
        return element;
    }

    unshift(...elements) {
        super.unshift(...elements);
        this.delayedSaveSync();
    }

    shift() {
        const element = super.shift();
        this.delayedSaveSync();
        return element;
    }

    set(index, value) {
        super[index] = value;
        this.delayedSaveSync();
    }

    splice(index, count) {
        super.splice(index, count);
        this.delayedSaveSync();
    }

    loadSync() {
        if (!fs.existsSync(this.filePath)) return this;
        const elements = JSON.parse(fs.readFileSync(this.filePath).toString());
        for (const e of elements)
            super.push(e);
        return this;
    }

    saveSync() {
        fs.writeFileSync(this.filePath, JSON.stringify(this));
        return this;
    }

    delayedSaveSync() {
        this.delayedAction.clearTimeout().fire(() => this.saveSync());
    }
}

module.exports = class CollectionDatabase {
    constructor(databaseName, baseDirectory) {
        this.baseDirectory = baseDirectory || path.join(USER_PATH, '.data');
        this.databaseName = databaseName;
    }

    async getCollection(name) {
        if (name in COLLECTION_CACHE)
            return COLLECTION_CACHE[name];

        ensureDirectoryExists(this.baseDirectory);

        const fileName = uuidByString(`${this.databaseName}.${name}`);
        const filePath = path.join(this.baseDirectory, fileName);
        const collection = new PersistentCollection(filePath).loadSync();
        COLLECTION_CACHE[name] = collection;
        return collection;
    }

    async dropCollection(name) {
        if (!(name in COLLECTION_CACHE)) return;
        
        const collection = COLLECTION_CACHE[name];
        for (let i = 0; i < collection.length; i++)
            collection.shift();
        collection.saveSync();
    }
};