
const CollectionDatabase = require('./CollectionDatabase');
const Logger = require('../Logger');

module.exports = class PersistenceLayer {
    constructor({ config }) {
        const basePath = config.database.path;
        const databaseName = config.database.name;
        this.database = new CollectionDatabase(databaseName, basePath);
        this.logger = new Logger('persistence-layer', config.logger.level);
    }

    async getCollection(name) {
        return this.database.getCollection(name);
    }

    async dropCollection(name) {
        return this.database.dropCollection(name);
    }
};