const Logger = require('../Logger');

const NAME = 'connections';

class DuplicateSSIDError extends Error {
    constructor(ssid) {
        super(`Duplicate SSID: "${ssid}"`);
        this.ssid = ssid;
    }
}

module.exports = class Connections {
    constructor({ persistenceLayer, connectionMapper, config }) {
        this.logger = new Logger('connections', config.logger.level);
        this.connectionMapper = connectionMapper;
        this.persistenceLayer = persistenceLayer;
    }

    async add(connection) {
        const collection = await this.persistenceLayer.getCollection(NAME);
        this.logger.debug({ tags: [ 'find-all', 'collection-loaded' ]});

        if (collection.findIndex(c => c.ssid === connection.ssid) > -1)
            throw new DuplicateSSIDError(connection.ssid);

        const data = this.connectionMapper.toData(connection);
        collection.push(data);
        return data.id;
    }

    async findAll() {
        const collection = await this.persistenceLayer.getCollection(NAME);
        this.logger.debug({ tags: [ 'find-all', 'collection-loaded' ]});
        return collection.map(c => this.connectionMapper.toEntity(c));
    }

    async findOneById(id) {
        const byId = c => c.id === id;
        const collection = await this.persistenceLayer.getCollection(NAME);
        this.logger.debug({ tags: [ 'find-one-by-ssid', 'collection-loaded' ]});
        return this.connectionMapper.toEntity(collection.find(byId));
    }

    async update(connection) {
        const byId = c => c.id === connection.id;
        const collection = await this.persistenceLayer.getCollection(NAME);
        this.logger.debug({ tags: [ 'update', 'collection-loaded' ]});

        const idx = collection.findIndex(byId);
        if (idx >= 0)
            collection.set(idx, this.connectionMapper.toData(connection));
    }

    async removeById(id) {
        const byId = c => c.id === id;
        const collection = await this.persistenceLayer.getCollection(NAME);
        this.logger.debug({ tags: [ 'remove-by-ssid', 'collection-loaded' ]});
        const idx = collection.findIndex(byId);
        collection.splice(idx, 1);
    }

    async purge() {
        await this.persistenceLayer.dropCollection(NAME);
        this.logger.debug({ tags: [ 'purge', 'collection-dropped' ]});
    }
};