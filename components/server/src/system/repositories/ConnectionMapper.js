const uuid = require('uuid');

module.exports = class ConnectionMapper {

    constructor({ connectionBuilder }) {
        this.connectionBuilder = connectionBuilder;
    }

    toEntity(data) {
        const attributes = this.connectionBuilder.begin();
        for (const name in data)
            Reflect.set(attributes, name, data[name]);
        return attributes.build();
    }

    toData(entity) {
        const data = {
            id: entity.id || uuid.v4(),
            ssid: entity.ssid,
            password: entity.password,
            hidden: entity.hidden
        };
        return data;
    }
};