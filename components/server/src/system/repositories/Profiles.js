const { ProfileConstants } = require('../../../../profile');

const Logger = require('../Logger');

const NAME = 'profiles';

const NAMES = {
    'HotspotProfile': ProfileConstants.PROFILE_NAME_HOTSPOT,
    'WifiProfile': ProfileConstants.PROFILE_NAME_WIFI
};

module.exports = class Profiles {
    constructor({ persistenceLayer, profileFactory, config }) {
        this.persistenceLayer = persistenceLayer;
        this.profileFactory = profileFactory;
        this.logger = new Logger('profiles', config.logger.level);
    }

    async getCurrent() {
        const collection = await this.persistenceLayer.getCollection(NAME);
        this.logger.debug({ tags: [ 'get-current', 'collection', 'loaded' ]});

        if (collection.length === 0)
            return null;

        this.logger.debug({ tags: [ 'get-current', 'collection', 'count=1' ]});

        if (collection[0] === ProfileConstants.PROFILE_NAME_HOTSPOT)
            return this.profileFactory.makeHotspotProfile();

        if (collection[0] === ProfileConstants.PROFILE_NAME_WIFI)
            return this.profileFactory.makeWifiProfile();

        return null;
    }

    async setCurrent(profile) {
        const collection = await this.persistenceLayer.getCollection(NAME);
        this.logger.debug({ tags: [ 'set-current', 'collection', 'loaded' ]});

        const name = NAMES[profile.constructor.name];

        if (collection.length === 0)
            collection.push(name);
        else
            collection.set(0, name);

        this.logger.debug({ tags: [ 'set-current', 'profile', 'set' ]});
    }
};