const Logger = require('./Logger');

module.exports = class Scanner {
    constructor() {
        this.logger = new Logger('scanner');
    }

    async scan() {
        this.logger.info('found one network');
        return true;
    }
};