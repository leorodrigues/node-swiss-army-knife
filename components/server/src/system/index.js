module.exports = {
    Logger: require('./Logger'),
    Scanner: require('./Scanner'),
    Profiles: require('./repositories/Profiles'),
    Receiver: require('./Receiver'),
    Connections: require('./repositories/Connections'),
    Transmitter: require('./Transmitter'),
    OperatingSystem: require('./OperatingSystem'),
    PersistenceLayer: require('./repositories/PersistenceLayer')
};