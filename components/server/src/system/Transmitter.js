const Logger = require('./Logger');

module.exports = class Transmitter {

    constructor() {
        this.logger = new Logger('transmitter');
    }

    async start() {
        this.logger.info('transmitter started');
    }
};