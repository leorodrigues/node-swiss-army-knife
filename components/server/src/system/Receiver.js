const Logger = require('./Logger');

module.exports = class Receiver {
    constructor() {
        this.logger = new Logger('receiver');
    }

    async start() {
        this.logger.info({ tags: [ 'startup', 'completed', 'success' ]});
    }
};