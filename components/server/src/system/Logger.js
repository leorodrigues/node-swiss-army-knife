
const { createLogger, transports, format } = require('winston');
const { colorize, timestamp, combine, printf } = format;

module.exports = class Logger {
    constructor(moduleName, level) {
        const plainFormat = Logger.makeFormatter(moduleName);
        const format = combine(colorize(), timestamp(), plainFormat);
        this.delegate = createLogger({
            level, transports: [ new transports.Console({ format }) ]
        });
    }

    debug(data) {
        this.delegate.debug(data);
    }

    info(data) {
        this.delegate.info(data);
    }

    warn(data) {
        this.delegate.warn(data);
    }

    error(data) {
        this.delegate.error(data);
    }

    static makeFormatter(label) {
        return printf(({ timestamp, level, message }) => {
            if (typeof(message) === 'string')
                return `${timestamp} ${level} ${label} message="${message}"`;
            const elements = [timestamp, level, label];
            if (message.tags) elements.push(...message.tags);
            if (message.map) elements.push(Logger.planify(message.map));
            return elements.join(' ');
        });
    }

    static planify(args) {
        const toKeyValuePair = k => `${k}=${JSON.stringify(args[k])}`;
        return Object.keys(args).map(toKeyValuePair).join(' ');
    }

    static kebabCase(text) {
        const pattern = /([a-z])([A-Z])/g;
        const replacer = (s, l, u) => `${l}-${u.toLowerCase()}`;
        return text.replace(pattern, replacer).toLowerCase();
    }
};