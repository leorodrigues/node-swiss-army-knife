const { WifiProfile } = require('../../../src/domain');
const { makeMocks } = require('./WifiProfile.mockery');

const { profileEvents } = makeMocks(jest);

const wifiProfile = new WifiProfile(profileEvents);

describe('components/profile/domain/WifiProfile', () => {
    describe('#fireProfileLoaded()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => wifiProfile.fireProfileLoaded());

        it('Should emit a "wifiProfileLoaded" event', () =>
            expect(profileEvents.emitWifiProfileLoaded)
                .toHaveBeenCalledWith());

    });
    describe('#fireProfileSwitched()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => wifiProfile.fireProfileSwitched());

        it('Should emit a "profileSwitched" event', () =>
            expect(profileEvents.emitProfileSwitched)
                .toHaveBeenCalledWith());
    });
});