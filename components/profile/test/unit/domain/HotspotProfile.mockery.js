function makeMocks(jest) {
    const stubs = {
        emitHotspotProfileLoaded: jest.fn(),
        emitWifiProfileLoaded: jest.fn(),
        emitProfileSwitched: jest.fn()
    };

    const profileEvents = {
        emitHotspotProfileLoaded: stubs.emitHotspotProfileLoaded,
        emitWifiProfileLoaded: stubs.emitWifiProfileLoaded,
        emitProfileSwitched: stubs.emitProfileSwitched
    };

    return { profileEvents };
}

module.exports = { makeMocks };