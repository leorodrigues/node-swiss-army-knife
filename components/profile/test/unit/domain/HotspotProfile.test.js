
const { HotspotProfile } = require('../../../src/domain');
const { makeMocks } = require('./HotspotProfile.mockery');

const { profileEvents } = makeMocks(jest);

const hotspotProfile = new HotspotProfile(profileEvents);

describe('components/profile/domain/HotspotProfile', () => {
    describe('#fireProfileLoaded()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => hotspotProfile.fireProfileLoaded());

        it('Should emit a "hotspotProfileLoaded" event', async () =>
            expect(profileEvents.emitHotspotProfileLoaded)
                .toHaveBeenCalledWith());
    });
    describe('#fireProfileSwitched()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => hotspotProfile.fireProfileSwitched());

        it('Should emit a "profileSwitched" event', async () =>
            expect(profileEvents.emitProfileSwitched)
                .toHaveBeenCalledWith());
    });
});