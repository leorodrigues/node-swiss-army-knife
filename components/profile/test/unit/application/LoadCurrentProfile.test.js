const { LoadCurrentProfile } = require('../../../src/application');
const { makeMocks } = require('./LoadCurrentProfile.mockery');

const { profile, profiles, profileFactory, stubs } = makeMocks(jest);

const loadCurrent = new LoadCurrentProfile({ profiles, profileFactory });

describe('components/profile/application/LoadCurrentProfile', () => {
    describe('#invoke()', () => {
        describe('Considering that a current profile has been set', () => {
            afterAll(() => jest.resetAllMocks());
            beforeAll(async () => {
                stubs.getCurrent.mockResolvedValue(profile);
                await loadCurrent.invoke();
            });

            it('Should load the current profile', async () =>
                expect(profiles.getCurrent).toHaveBeenCalledWith());

            it('Should fire the "profileLoaded" event', async () =>
                expect(profile.fireProfileLoaded).toHaveBeenCalledWith());

            it('Should skip creation of a new hotspot profile', async () =>
                expect(profileFactory.makeHotspotProfile)
                    .toHaveBeenCalledTimes(0));

            it('Should skip setting the current profile', async () =>
                expect(profiles.setCurrent).toHaveBeenCalledTimes(0));
        });

        describe('Considering that there is no current profile set', () => {
            afterAll(() => jest.resetAllMocks());
            beforeAll(async () => {
                stubs.getCurrent.mockResolvedValue(null);
                stubs.makeHotspotProfile.mockReturnValue(profile);
                await loadCurrent.invoke();
            });

            it('Should load the current profile', async () =>
                expect(profiles.getCurrent).toHaveBeenCalledWith());

            it('Should fabricate a hotspot profile', async () =>
                expect(profileFactory.makeHotspotProfile)
                    .toHaveBeenCalledWith());

            it('Should set the current profile', async () =>
                expect(profiles.setCurrent).toHaveBeenCalledWith(profile));

            it('Should fire the "profileLoaded" event', async () =>
                expect(profile.fireProfileLoaded).toHaveBeenCalledWith());
        });
    });
});