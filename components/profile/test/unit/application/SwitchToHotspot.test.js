const { SwitchToHotspot } = require('../../../src/application');
const { makeMocks } = require('./SwitchToHotspot.mockery');

const { profile, profiles, profileFactory, stubs } = makeMocks(jest);

const switchToHotspot = new SwitchToHotspot({ profiles, profileFactory });

describe('components/profile/application/SwitchToHotspot', () => {
    describe('#invoke()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(async () => {
            stubs.makeHotspotProfile.mockReturnValue(profile);
            await switchToHotspot.invoke();
        });

        it('Should instantiate a new "HotspotProfile"', async () =>
            expect(profileFactory.makeHotspotProfile)
                .toHaveBeenCalledWith());

        it('Should set the current profile', async () =>
            expect(profiles.setCurrent)
                .toHaveBeenCalledWith(profile));

        it('Should fire the "profileSwitched" event', async () =>
            expect(profile.fireProfileSwitched).toHaveBeenCalledWith());
    });
});