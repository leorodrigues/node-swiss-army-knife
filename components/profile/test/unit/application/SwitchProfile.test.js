
const { SwitchProfile } = require('../../../src/application');
const { ProfileConstants, UnknownProfileNameError } =
    require('../../../src/domain');
const { makeMocks } = require('./SwitchProfile.mockery');

const { switchToHotspot, switchToWifi } = makeMocks(jest);

const subject = new SwitchProfile({ switchToHotspot, switchToWifi });

describe('components/profile/application/SwitchProfile', () => {
    describe('#invoke(profileName)', () => {
        it('Should invoke "switchToWifi"', async () => {
            await subject.invoke(ProfileConstants.PROFILE_NAME_WIFI);
            expect(switchToWifi.invoke).toHaveBeenCalledWith();
        });

        it('Should invoke "switchToHotspot"', async () => {
            await subject.invoke(ProfileConstants.PROFILE_NAME_HOTSPOT);
            expect(switchToHotspot.invoke).toHaveBeenCalledWith();
        });

        it('Should throw an exception', async () => {
            try {
                await subject.invoke('refrigerator');

                /* istanbul ignore next */
                throw new Error('Execution should not have come this far!');
            } catch(error) {
                expect(error).toBeInstanceOf(UnknownProfileNameError);
            }
        });
    });
});