const { SwitchToWifi } = require('../../../src/application');
const { makeMocks } = require('./SwitchToWifi.mockery');

const { profile, profiles, profileFactory, stubs } = makeMocks(jest);

const switchToWifi = new SwitchToWifi({ profiles, profileFactory });

describe('components/profile/application/SwitchToWifi', () => {
    describe('#invoke()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(async () => {
            stubs.makeWifiProfile.mockReturnValue(profile);
            await switchToWifi.invoke();
        });

        it('Should instantiate a new "WifiProfile"', async () =>
            expect(profileFactory.makeWifiProfile).toHaveBeenCalledWith());

        it('Should set the current profile', async () =>
            expect(profiles.setCurrent).toHaveBeenCalledWith(profile));

        it('Should fire the "profileSwitched" event', async () =>
            expect(profile.fireProfileSwitched).toHaveBeenCalledWith());
    });
});