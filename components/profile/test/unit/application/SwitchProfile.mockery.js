
function makeMocks(jest) {

    const stubs = {
        invoke: jest.fn()
    };

    const switchToHotspot = {
        invoke: stubs.invoke
    };

    const switchToWifi = {
        invoke: stubs.invoke
    };

    return { switchToWifi, switchToHotspot, stubs };
}

module.exports = { makeMocks };