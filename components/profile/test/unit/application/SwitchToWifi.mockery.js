function makeMocks(jest) {
    const stubs = {
        fireProfileLoaded: jest.fn(),
        fireProfileSwitched: jest.fn(),
        getCurrent: jest.fn(),
        setCurrent: jest.fn(),
        makeHotspotProfile: jest.fn(),
        makeWifiProfile: jest.fn()
    };

    const profile = {
        fireProfileLoaded: stubs.fireProfileLoaded,
        fireProfileSwitched: stubs.fireProfileSwitched
    };

    const profiles = {
        getCurrent: stubs.getCurrent,
        setCurrent: stubs.setCurrent
    };

    const profileFactory = {
        makeHotspotProfile: stubs.makeHotspotProfile,
        makeWifiProfile: stubs.makeWifiProfile
    };

    return { profile, profiles, profileFactory, stubs };
}

module.exports = { makeMocks };