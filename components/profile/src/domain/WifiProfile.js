module.exports = class WifiProfile {
    constructor(profileEvents) {
        this.profileEvents = profileEvents;
    }

    async fireProfileLoaded() {
        this.profileEvents.emitWifiProfileLoaded();
    }

    async fireProfileSwitched() {
        this.profileEvents.emitProfileSwitched();
    }
};