
module.exports = class HotspotProfile {
    constructor(profileEvents) {
        this.profileEvents = profileEvents;
    }

    async fireProfileLoaded() {
        this.profileEvents.emitHotspotProfileLoaded();
    }

    async fireProfileSwitched() {
        this.profileEvents.emitProfileSwitched();
    }
};