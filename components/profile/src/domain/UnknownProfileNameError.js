module.exports = class UnknownProfileNameError extends Error {
    constructor(profileName) {
        super(`There is no such profile: "${profileName}"`);
    }
};