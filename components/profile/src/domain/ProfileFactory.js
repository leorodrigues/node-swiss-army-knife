
const HotspotProfile = require('./HotspotProfile');
const WifiProfile = require('./WifiProfile');

/* istanbul ignore next */
module.exports = class ProfileFactory {
    constructor({ profileEvents }) {
        this.profileEvents = profileEvents;
    }

    makeHotspotProfile() {
        return new HotspotProfile(this.profileEvents);
    }

    makeWifiProfile() {
        return new WifiProfile(this.profileEvents);
    }
};