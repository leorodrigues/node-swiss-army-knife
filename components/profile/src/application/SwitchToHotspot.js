
module.exports = class SwitchToHotspot {
    constructor({ profiles, profileFactory }) {
        this.profileFactory = profileFactory;
        this.profiles = profiles;
    }

    async invoke() {
        const profile = this.profileFactory.makeHotspotProfile();
        await this.profiles.setCurrent(profile);
        return profile.fireProfileSwitched();
    }
};