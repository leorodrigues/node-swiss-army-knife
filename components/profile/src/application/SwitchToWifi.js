
module.exports = class SwitchToWifi {
    constructor({ profiles, profileFactory }) {
        this.profileFactory = profileFactory;
        this.profiles = profiles;
    }

    async invoke() {
        const profile = this.profileFactory.makeWifiProfile();
        await this.profiles.setCurrent(profile);
        profile.fireProfileSwitched();
    }
};