const { UnknownProfileNameError } = require('../domain');
const { ProfileConstants } = require('../domain');

const { PROFILE_NAME_HOTSPOT, PROFILE_NAME_WIFI } = ProfileConstants;

module.exports = class SwitchProfile {
    constructor({ switchToHotspot, switchToWifi }) {
        this.switchToHotspot = switchToHotspot;
        this.switchToWifi = switchToWifi;
    }

    async invoke(profileName) {
        if (profileName === PROFILE_NAME_HOTSPOT)
            return this.switchToHotspot.invoke();
        if (profileName === PROFILE_NAME_WIFI)
            return this.switchToWifi.invoke();
        throw new UnknownProfileNameError(profileName);
    }
};