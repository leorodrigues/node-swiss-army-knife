module.exports = class LoadCurrentProfile {

    constructor({ profiles, profileFactory }) {
        this.profileFactory = profileFactory;
        this.profiles = profiles;
    }

    async invoke() {
        let profile = await this.profiles.getCurrent();
        if (!profile) {
            profile = this.profileFactory.makeHotspotProfile();
            await this.profiles.setCurrent(profile);
        }
        profile.fireProfileLoaded();
    }
};