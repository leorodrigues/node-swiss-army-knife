module.exports = {
    LoadCurrentProfile: require('./LoadCurrentProfile'),
    SwitchToHotspot: require('./SwitchToHotspot'),
    SwitchProfile: require('./SwitchProfile'),
    SwitchToWifi: require('./SwitchToWifi')
};