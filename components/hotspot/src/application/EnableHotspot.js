
module.exports = class EnableHotspot {
    constructor({ hotspotFactory }) {
        this.hotspotFactory = hotspotFactory;
    }

    async invoke() {
        const hotspot = this.hotspotFactory.makeHotspot();
        return hotspot.enable();
    }
};