
const Hotspot = require('./Hotspot');

/* istanbul ignore next */
module.exports = class HotspotFactory {
    constructor({ receiver, hotspotEvents }) {
        this.receiver = receiver;
        this.hotspotEvents = hotspotEvents;
    }

    makeHotspot() {
        return new Hotspot(this.receiver, this.hotspotEvents);
    }
};