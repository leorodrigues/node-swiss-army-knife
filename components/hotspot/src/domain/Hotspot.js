
module.exports = class Hotspot {
    constructor(receiver, hotspotEvents) {
        this.receiver = receiver;
        this.hotspotEvents = hotspotEvents;
    }

    async enable() {
        await this.receiver.start();
        return this.hotspotEvents.emitHotspotEnabled();
    }
};