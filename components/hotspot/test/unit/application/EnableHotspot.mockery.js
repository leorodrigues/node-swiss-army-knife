function makeMocks(jest) {

    const stubs = {
        makeHotspot: jest.fn(),
        enable: jest.fn(),
    };

    const hotspotFactory = {
        makeHotspot: stubs.makeHotspot
    };

    const hotspot = {
        enable: stubs.enable
    };

    return { hotspotFactory, hotspot, stubs };
}

module.exports = { makeMocks };