const { EnableHotspot } = require('../../../src/application');
const { makeMocks } = require('./EnableHotspot.mockery');

const { hotspotFactory, hotspot, stubs } = makeMocks(jest);

const enable = new EnableHotspot({ hotspotFactory });

describe('components/hotspot/application/EnableHotspot', () => {
    describe('#invoke()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(async () => {
            stubs.makeHotspot.mockReturnValue(hotspot);
            await enable.invoke();
        });

        it('Should instantiate a hotspot', async () =>
            expect(hotspotFactory.makeHotspot).toHaveBeenCalledWith());

        it('Should enable the instantiated hotspot', async () =>
            expect(hotspot.enable).toHaveBeenCalledWith());
    });
});