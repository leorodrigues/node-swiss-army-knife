
function makeMocks(jest) {

    const stubs = {
        start: jest.fn(),
        emitHotspotEnabled: jest.fn(),
    };

    const receiver = {
        start: stubs.start,
        password: 'refrigerator'
    };

    const hotspotEvents = {
        emitHotspotEnabled: stubs.emitHotspotEnabled
    };

    return { receiver, hotspotEvents, stubs };
}

module.exports = { makeMocks };