const { Hotspot } = require('../../../src/domain');
const { makeMocks } = require('./Hotspot.mockery');

const { receiver, hotspotEvents } = makeMocks(jest);

const hotspot = new Hotspot(receiver, hotspotEvents);

describe('components/profile/domain/Hotspot', () => {
    describe('#enable()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => hotspot.enable());

        it('Should start the receiver', async () =>
            expect(receiver.start).toHaveBeenCalledWith());

        it('Should emit the "hotspotEnabled" event', async () =>
            expect(hotspotEvents.emitHotspotEnabled).toHaveBeenCalledWith());
    });
});