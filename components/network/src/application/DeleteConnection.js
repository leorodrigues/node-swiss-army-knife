module.exports = class DeleteConnection {
    constructor({ connections }) {
        this.connections = connections;
    }

    async invoke(id) {
        return this.connections.removeById(id);
    }
};