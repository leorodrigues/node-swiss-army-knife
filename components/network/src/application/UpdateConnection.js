
function anyDefined(...parameters) {
    return parameters.some(p => p !== undefined);
}

module.exports = class UpdateConnection {
    constructor({ connections }) {
        this.connections = connections;
    }

    async invoke(id, { ssid, password, hidden }) {
        const connection = await this.connections.findOneById(id);

        if (connection === null)
            return null;

        if (anyDefined(ssid))
            connection.ssid = ssid;

        if (anyDefined(password))
            connection.password = password;

        if (anyDefined(hidden))
            connection.hidden = hidden;

        if (anyDefined(ssid, password, hidden)) {
            await this.connections.update(connection);
            return connection;
        }

        return null;
    }
};