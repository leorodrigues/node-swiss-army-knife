module.exports = {
    AcquireNetwork: require('./AcquireNetwork'),
    DeleteConnection: require('./DeleteConnection'),
    CreateConnection: require('./CreateConnection'),
    GetConnections: require('./GetConnections'),
    UpdateConnection: require('./UpdateConnection')
};