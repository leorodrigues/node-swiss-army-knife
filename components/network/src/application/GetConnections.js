module.exports = class GetNetworks {
    constructor({ connections }) {
        this.connections = connections;
    }

    async invoke() {
        return this.connections.findAll();
    }
};