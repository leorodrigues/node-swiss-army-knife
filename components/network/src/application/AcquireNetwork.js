
module.exports = class AcquireNetwork {
    constructor({ networkFactory }) {
        this.networkFactory = networkFactory;
    }

    async invoke() {
        const network = this.networkFactory.makeNetwork();
        return network.acquire();
    }
};