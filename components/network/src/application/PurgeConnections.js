module.exports = class PurgeConnections {
    constructor({ connections }) {
        this.connections = connections;
    }

    async invoke() {
        this.connections.purge();
    }
};