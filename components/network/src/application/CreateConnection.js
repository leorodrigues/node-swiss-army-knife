const { Connection } = require('../domain');

module.exports = class CreateConnection {
    constructor({ connections }) {
        this.connections = connections;
    }

    async invoke(ssid, password, hidden) {
        const entity = new Connection(ssid, password, hidden);
        entity.id = await this.connections.add(entity);
        return entity;
    }
};