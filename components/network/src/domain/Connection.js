module.exports = class Connection {
    constructor(ssid, password, hidden, id) {
        this.ssid = ssid;
        this.password = password;
        this.hidden = hidden;
        this.id = id;
    }
};