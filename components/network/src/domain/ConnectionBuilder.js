
const Connection = require('./Connection');

class Attributes {

    constructor() {
        this.values = { };
    }

    set ssid(ssid) {
        this.values.ssid = ssid;
    }

    set password(password) {
        this.values.password = password;
    }

    set hidden(hidden) {
        this.values.hidden = hidden;
    }

    set id(id) {
        this.values.id = id;
    }

    build() {
        if (this.values.id)
            return new Connection(
                this.values.ssid,
                this.values.password,
                this.values.hidden,
                this.values.id);
        return new Connection(
            this.values.ssid,
            this.values.password,
            this.values.hidden);
    }
}

module.exports = class ConnectionBuilder {
    constructor() {
        this.attributes = { };
    }

    begin() {
        return new Attributes();
    }
};