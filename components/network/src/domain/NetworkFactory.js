const Network = require('./Network');

/* istanbul ignore next */
module.exports = class NetworkFactory {
    constructor({ scanner, networkEvents }) {
        this.scanner = scanner;
        this.networkEvents = networkEvents;
    }

    makeNetwork() {
        return new Network(this.scanner, this.networkEvents);
    }
};