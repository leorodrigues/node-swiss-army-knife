module.exports = class Network {
    constructor(scanner, networkEvents) {
        this.scanner = scanner;
        this.networkEvents = networkEvents;
    }

    async acquire() {
        const networkAcquired = await this.scanner.scan();
        return networkAcquired
            ? this.networkEvents.emitNetworkAquired()
            : this.networkEvents.emitNetworkAquisitionFailed();
    }
};