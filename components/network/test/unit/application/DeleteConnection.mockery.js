
function makeMocks(jest) {
    const connections = {
        removeById: jest.fn()
    };

    return { connections };
}

module.exports = { makeMocks };