function makeMocks(jest) {
    const connections = { add: jest.fn() };
    return { connections };
}

module.exports = { makeMocks };