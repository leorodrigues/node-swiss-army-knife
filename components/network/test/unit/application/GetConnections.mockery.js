
function makeMocks(jest) {
    const connections = {
        findAll: jest.fn()
    };

    return { connections };
}

module.exports = { makeMocks };