
function makeMocks(jest) {
    const connections = {
        findOneById: jest.fn(),
        update: jest.fn()
    };

    return { connections };
}

module.exports = { makeMocks };