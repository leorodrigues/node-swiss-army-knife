
const { DeleteConnection } = require('../../../src/application');
const { makeMocks } = require('./DeleteConnection.mockery');

const { connections } = makeMocks(jest);

describe('components/network/DeleteConnection', () => {
    describe('#invoke(id)', () => {
        it('Should remove from the connection repository by id', async () => {
            const subject = new DeleteConnection({ connections });
            await subject.invoke('112358');
            expect(connections.removeById).toHaveBeenCalledWith('112358');
        });
    });
});