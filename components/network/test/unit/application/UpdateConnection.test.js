
const { UpdateConnection } = require('../../../src/application');
const { makeMocks } = require('./UpdateConnection.mockery');

const { connections } = makeMocks(jest);

describe('components/network/UpdateConnection', () => {
    describe('#invoke(id)', () => {
        afterEach(() => jest.resetAllMocks());

        it('Should update only the "ssid" attribute', async () => {
            const originalInstance = {
                ssid: 'old-ssid', password: 'old-secret', hidden: 1
            };

            connections.findOneById.mockResolvedValueOnce(originalInstance);

            const subject = new UpdateConnection({ connections });
            const result = await subject.invoke('112358', { ssid: 'new-ssid' });

            expect(result === originalInstance).toBeTruthy();
            expect(connections.findOneById).toHaveBeenCalledWith('112358');
            expect(connections.update).toHaveBeenCalledWith({
                ssid: 'new-ssid', password: 'old-secret', hidden: 1
            });
        });

        it('Should update only the "password" attribute', async () => {
            const originalInstance = {
                ssid: 'old-ssid', password: 'old-secret', hidden: 1
            };
            connections.findOneById.mockResolvedValueOnce(originalInstance);

            const subject = new UpdateConnection({ connections });
            const result = await subject.invoke('112358', { password: 'new-secret' });

            expect(result === originalInstance).toBeTruthy();
            expect(connections.findOneById).toHaveBeenCalledWith('112358');
            expect(connections.update).toHaveBeenCalledWith({
                ssid: 'old-ssid', password: 'new-secret', hidden: 1
            });
        });

        it('Should update only the "hidden" attribute', async () => {
            const originalInstance = {
                ssid: 'old-ssid', password: 'old-secret', hidden: 1
            };
            connections.findOneById.mockResolvedValueOnce(originalInstance);

            const subject = new UpdateConnection({ connections });
            const result = await subject.invoke('112358', { hidden: 7 });

            expect(result === originalInstance).toBeTruthy();
            expect(connections.findOneById).toHaveBeenCalledWith('112358');
            expect(connections.update).toHaveBeenCalledWith({
                ssid: 'old-ssid', password: 'old-secret', hidden: 7
            });
        });

        it('Should do nothing if the id is not found', async () => {
            connections.findOneById.mockResolvedValueOnce(null);

            const subject = new UpdateConnection({ connections });
            const result = await subject.invoke('112358', { });

            expect(result).toBeNull();
            expect(connections.findOneById).toHaveBeenCalledWith('112358');
            expect(connections.update).not.toHaveBeenCalled();
        });

        it('Should do nothing if the parameters are all undefined', async () => {
            connections.findOneById.mockResolvedValueOnce({ });

            const subject = new UpdateConnection({ connections });
            const result = await subject.invoke('112358', { });

            expect(result).toBeNull();
            expect(connections.findOneById).toHaveBeenCalledWith('112358');
            expect(connections.update).not.toHaveBeenCalled();
        });
    });
});