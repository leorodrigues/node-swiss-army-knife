const { AcquireNetwork } = require('../../../src/application');
const { makeMocks } = require('./AcquireNetwork.mockery');

const { networkFactory, network, stubs } = makeMocks(jest);

const acquire = new AcquireNetwork({ networkFactory });

describe('components/network/application/AcquireNetwork', () => {
    describe('#invoke()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(async () => {
            stubs.makeNetwork.mockReturnValue(network);
            await acquire.invoke();
        });

        it('Should instantiate the network object', async () =>
            expect(networkFactory.makeNetwork).toHaveBeenCalledWith());

        it('Should invoke the "acquire" behavior', async () =>
            expect(network.acquire).toHaveBeenCalledWith());
    });
});