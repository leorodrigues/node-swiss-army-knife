
const { GetConnections } = require('../../../src/application');
const { makeMocks } = require('./GetConnections.mockery');

const { connections } = makeMocks(jest);

describe('components/network/GetConnections', () => {
    describe('#invoke(id)', () => {
        it('Should remove from the connection repository by id', async () => {
            const subject = new GetConnections({ connections });
            await subject.invoke();
            expect(connections.findAll).toHaveBeenCalledWith();
        });
    });
});