const { CreateConnection } = require('../../../src/application');
const { Connection } = require('../../../src/domain');
const { makeMocks } = require('./CreateConnection.mockery');

const { connections } = makeMocks(jest);

const createConnection = new CreateConnection({ connections });

describe('components/network/application/CreateConnection', () => {
    describe('#invoke(ssid, password, hidden)', () => {
        beforeAll(async () => createConnection.invoke('my-net', '1234', true));
        it('Should save a new instance of "Connection"', async () =>
            expect(connections.add).toHaveBeenCalledWith(
                new Connection('my-net', '1234', true)));
    });
});