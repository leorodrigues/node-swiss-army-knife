
function makeMocks(jest) {
    const stubs = {
        makeNetwork: jest.fn(),
        acquire: jest.fn()
    };

    const networkFactory = {
        makeNetwork: stubs.makeNetwork
    };

    const network = {
        acquire: stubs.acquire
    };

    return { networkFactory, network, stubs };
}

module.exports = { makeMocks };