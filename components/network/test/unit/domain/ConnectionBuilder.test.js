
const { ConnectionBuilder, Connection } = require('../../../src/domain');

describe('components/network/comain/ConnectionBuilder', () => {
    describe('#build()', () => {
        it('Should build an instance w/o "id"', () => {
            const subject = new ConnectionBuilder().begin();

            subject.ssid = 'my-network';
            subject.password = 'secret';
            subject.hidden = true;

            expect(subject.build())
                .toStrictEqual(new Connection('my-network', 'secret', true));
        });
        it('Should build an instance w/ "id"', () => {
            const expected = new Connection('my-network', 'secret', true, 'HECBAA');
            const subject = new ConnectionBuilder().begin();

            subject.id = 'HECBAA';
            subject.ssid = 'my-network';
            subject.password = 'secret';
            subject.hidden = true;

            expect(subject.build()).toStrictEqual(expected);
        });
    });
});