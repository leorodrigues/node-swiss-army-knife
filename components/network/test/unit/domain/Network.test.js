const { Network } = require('../../../src/domain');
const { makeMocks } = require('./Network.mockery');

const { scanner, networkEvents, stubs } = makeMocks(jest);

const network = new Network(scanner, networkEvents);

describe('components/network/domain/Network', () => {
    describe('#acquire()', () => {
        describe('Considering that a network was acquired', () => {
            afterAll(() => jest.resetAllMocks());
            beforeAll(() => {
                stubs.scan.mockResolvedValue(true);
                return network.acquire();
            });

            it('Should scan for networks', async () =>
                expect(scanner.scan).toHaveBeenCalledWith());

            it('Should emit the "networkAcquired" event', async () =>
                expect(networkEvents.emitNetworkAquired)
                    .toHaveBeenCalledWith());

            it('Should skip the "networkAcquisitionFailed"', async () =>
                expect(networkEvents.emitNetworkAquisitionFailed)
                    .toHaveBeenCalledTimes(0));
        });

        describe('Considering that no network was acquired', () => {
            afterAll(() => jest.resetAllMocks());
            beforeAll(() => {
                stubs.scan.mockResolvedValue(false);
                return network.acquire();
            });

            it('Should scan for networks', async () =>
                expect(scanner.scan).toHaveBeenCalledWith());

            it('Should skip the "networkAcquired" event', async () =>
                expect(networkEvents.emitNetworkAquired)
                    .toHaveBeenCalledTimes(0));

            it('Should emit "networkAcquisitionFailed"', async () =>
                expect(networkEvents.emitNetworkAquisitionFailed)
                    .toHaveBeenCalledWith());
        });
    });
});