function makeMocks(jest) {
    const stubs = {
        scan: jest.fn(),
        emitNetworkAquired: jest.fn(),
        emitNetworkAquisitionFailed: jest.fn()
    };

    const scanner = {
        scan: stubs.scan
    };

    const networkEvents = {
        emitNetworkAquired: stubs.emitNetworkAquired,
        emitNetworkAquisitionFailed: stubs.emitNetworkAquisitionFailed
    };

    return { scanner, networkEvents, stubs };
}

module.exports = { makeMocks };