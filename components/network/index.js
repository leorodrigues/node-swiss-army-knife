module.exports = {
    ...require('./src/application'),
    ...require('./src/domain')
};
