const { FireShutdown } = require('../../../src/application');
const { makeMocks } = require('./FireShutdown.mockery');

const { bootSequenceFactory, bootSequence, stubs } = makeMocks(jest);

const fireShutdown = new FireShutdown({ bootSequenceFactory });

describe('components/boot-sequence/application/FireShutdown', () => {
    afterAll(() => jest.resetAllMocks());
    beforeAll(async () => {
        stubs.getInstance.mockReturnValue(bootSequence);
        await fireShutdown.invoke();
    });

    describe('#invoke()', () => {
        it('Should get the "BootSequence" instance', async () =>
            expect(bootSequenceFactory.getInstance).toHaveBeenCalledWith());

        it('Should ask the instance to fire the event', async () =>
            expect(bootSequence.fireShutdown).toHaveBeenCalledWith());
    });
});