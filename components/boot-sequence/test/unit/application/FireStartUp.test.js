const { FireStartUp } = require('../../../src/application');
const { makeMocks } = require('./FireStartUp.mockery');

const { bootSequenceFactory, bootSequence, stubs } = makeMocks(jest);

const fireStartUp = new FireStartUp({ bootSequenceFactory });

describe('components/boot-sequence/application/FireStartUp', () => {
    afterAll(() => jest.resetAllMocks());
    beforeAll(async () => {
        stubs.getInstance.mockReturnValue(bootSequence);
        await fireStartUp.invoke();
    });

    describe('#invoke()', () => {
        it('Should get the "BootSequence" instance', async () =>
            expect(bootSequenceFactory.getInstance).toHaveBeenCalledWith());

        it('Should ask the instance to fire the event', async () =>
            expect(bootSequence.fireStartUp).toHaveBeenCalledWith());
    });
});