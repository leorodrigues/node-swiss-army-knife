function makeMocks(jest) {
    const stubs = {
        getInstance: jest.fn(),
        fireShutdown: jest.fn(),
        fireStartUp: jest.fn(),
        fireReboot: jest.fn(),
    };

    const bootSequenceFactory = {
        getInstance: stubs.getInstance
    };

    const bootSequence = {
        fireShutdown: stubs.fireShutdown,
        fireStartUp: stubs.fireStartUp,
        fireReboot: stubs.fireReboot
    };

    return { bootSequenceFactory, bootSequence, stubs };
}

module.exports = { makeMocks };