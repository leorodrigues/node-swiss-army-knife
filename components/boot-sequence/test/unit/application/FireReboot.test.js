const { FireReboot } = require('../../../src/application');
const { makeMocks } = require('./FireReboot.mockery');

const { bootSequenceFactory, bootSequence, stubs  } = makeMocks(jest);

const fireReboot = new FireReboot({ bootSequenceFactory });

describe('components/boot-sequence/application/FireReboot', () => {
    describe('#invoke()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(async () => {
            stubs.getInstance.mockReturnValue(bootSequence);
            await fireReboot.invoke();
        });

        it('Should get the "BootSequence" instance', async () =>
            expect(bootSequenceFactory.getInstance).toBeCalledWith());

        it('Should ask the instance to fire the event', async () =>
            expect(bootSequence.fireReboot).toBeCalledWith());
    });
});