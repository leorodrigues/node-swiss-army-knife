
const { BootSequence } = require('../../../src/domain');
const { makeMocks } = require('./BootSequence.mockery');

const { operatingSystem, bootSequenceEvents } = makeMocks(jest);

const bootSequence = new BootSequence(operatingSystem, bootSequenceEvents);

describe('components/boot-sequence/domain/BootSequence', () => {
    describe('#fireReboot()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => bootSequence.fireReboot());

        it('Should schedule a reboot operation', async () =>
            expect(operatingSystem.scheduleReboot)
                .toHaveBeenCalledWith(BootSequence.REBOOT_DELAY));

        it('Should emit the "rebootScheduled" event', async () =>
            expect(bootSequenceEvents.emitRebootScheduled)
                .toHaveBeenCalledWith());
    });

    describe('#fireShutdown()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => bootSequence.fireShutdown());

        it('Should schedule a shutdown operation', async () =>
            expect(operatingSystem.scheduleShutdown)
                .toHaveBeenCalledWith(BootSequence.REBOOT_DELAY));

        it('Should emit the "shutdownScheduled" event', async () =>
            expect(bootSequenceEvents.emitShutdownScheduled)
                .toHaveBeenCalledWith());
    });

    describe('#fireStartUp()', () => {
        afterAll(() => jest.resetAllMocks());
        beforeAll(() => bootSequence.fireStartUp());

        it('Should emit the "startUpComplete" event', async () =>
            expect(bootSequenceEvents.emitStartUpComplete)
                .toHaveBeenCalledWith());
    });
});