function makeMocks(jest) {

    const operatingSystem = {
        scheduleReboot: jest.fn(),
        scheduleShutdown: jest.fn()
    };

    const bootSequenceEvents = {
        emitRebootScheduled: jest.fn(),
        emitShutdownScheduled: jest.fn(),
        emitStartUpComplete: jest.fn()
    };

    return { bootSequenceEvents, operatingSystem };
}

module.exports = { makeMocks };