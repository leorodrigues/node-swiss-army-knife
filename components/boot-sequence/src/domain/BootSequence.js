
module.exports = class BootSequence {
    constructor(operatingSystem, eventSource) {
        this.operatingSystem = operatingSystem;
        this.bootSequenceEvents = eventSource;
    }

    static get REBOOT_DELAY() {
        return 5;
    }

    async fireReboot() {
        await this.operatingSystem.scheduleReboot(BootSequence.REBOOT_DELAY);
        this.bootSequenceEvents.emitRebootScheduled();
    }

    async fireShutdown() {
        await this.operatingSystem.scheduleShutdown(BootSequence.REBOOT_DELAY);
        this.bootSequenceEvents.emitShutdownScheduled();
    }

    async fireStartUp() {
        this.bootSequenceEvents.emitStartUpComplete();
    }
};