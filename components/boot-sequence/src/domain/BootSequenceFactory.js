const BootSequence = require('./BootSequence');

/* istanbul ignore next */
module.exports = class BootSequenceFactory {
    constructor({ operatingSystem,  bootSequenceEvents }) {
        this.operatingSystem = operatingSystem;
        this.bootSequenceEvents = bootSequenceEvents;
    }

    getInstance() {
        return new BootSequence(this.operatingSystem, this.bootSequenceEvents);
    }
};
