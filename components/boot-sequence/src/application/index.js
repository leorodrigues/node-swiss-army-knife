module.exports = {
    FireReboot: require('./FireReboot'),
    FireShutdown: require('./FireShutdown'),
    FireStartUp: require('./FireStartUp')
};