
module.exports = class FireReboot {
    constructor({ bootSequenceFactory }) {
        this.bootSequenceFactory = bootSequenceFactory;
    }

    async invoke() {
        const bootSequence = await this.bootSequenceFactory.getInstance();
        bootSequence.fireReboot();
    }
};
