
module.exports = class FireStartUp {
    constructor({ bootSequenceFactory }) {
        this.bootSequenceFactory = bootSequenceFactory;
    }

    async invoke() {
        const bootSequence = await this.bootSequenceFactory.getInstance();
        bootSequence.fireStartUp();
    }
};
