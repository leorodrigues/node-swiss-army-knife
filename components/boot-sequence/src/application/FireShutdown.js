
module.exports = class FireShutdown {
    constructor({ bootSequenceFactory }) {
        this.bootSequenceFactory = bootSequenceFactory;
    }

    async invoke() {
        const bootSequence = await this.bootSequenceFactory.getInstance();
        bootSequence.fireShutdown();
    }
};
