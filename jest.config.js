module.exports = {
    roots: [ './components', './lib' ],
    testRegex: '.*/(components|lib|app)/.*/test/unit/.*.test.js',
    moduleFileExtensions: [ 'js' ],
    coverageReporters: [ 'lcov', 'text-summary' ]
};